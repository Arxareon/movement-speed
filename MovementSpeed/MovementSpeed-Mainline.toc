## Interface: 90205
## Title: Movement Speed
## Version: 2.2.5
## X-Day: 9
## X-Month: 7
## X-Year: 2022
## Author: Barnabas Nagy (Arxareon)
## X-License: All Rights Reserved
## Notes: View the movement speed of anyone in customizable displays and tooltips with fast updates.
## SavedVariables: MovementSpeedDB, MovementSpeedCS
## SavedVariablesPerCharacter: MovementSpeedDBC

WidgetTools/WidgetTools.lua
MovementSpeedLocalizations.lua
MovementSpeed.lua